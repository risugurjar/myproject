import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-share',
  templateUrl: 'share.html',
})
export class SharePage {

  urllnk:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private socialSharing: SocialSharing,) {

    this.urllnk="https://play.google.com/store/apps/details?id=com.di.project";
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SharePage');
  }

  onshare(med:string)
  {
    if(med=='share')
    {
      this.socialSharing.share("share",null,null,"https://play.google.com/store/apps/details?id=com.di.project");
    }
    // if (med == 'gmail') {
    //   // Share via email
    //   this.socialSharing.canShareVia("com.google.android.gm", "Welcome to IBA. Click here for download the IBA App.", "IBA Mobile Application", null,this.urllnk).then(() => {
    //     this.socialSharing.shareVia("com.google.android.gm", "Welcome to IBA. Click here for download the IBA App.", "IBA Mobile Application", null,this.urllnk).then(() => {
    //       console.log("Success");
    //     }).catch((error) => {
    //       this.constant.ToastCustom(" Gmail not installed your device",'bottom');
    //       console.log("error " + JSON.stringify(error));
    //     });
    //   }).catch((error) => {
    //     this.constant.ToastCustom("Gmail not installed your device",'bottom');
    //     console.log("Error, Looks like you don't have Gmail app installed " + JSON.stringify(error) );
    //   });
    // }
    if(med=='facebook')
    {
      //alert( this.urllnk);
      this.socialSharing.shareViaFacebook("Welcome to India GK. Click here for Share on Facebook.", null,"https://play.google.com/store/apps/details?id=com.di.project").then((result) => {
      }).catch((msg) => {
        // this.constant.ToastCustom("Facebook not installed your device",'bottom');
      }); 
    }
    // if(med=='twitter')
    // {
    //   this.socialSharing.shareViaTwitter("Welcome to IBA. Click here for download the IBA App.",null,  this.urllnk).then((result) => {
    //    }).catch((msg) => {
    //     this.constant.ToastCustom("Twitter not installed your device",'bottom');
    //   }); 
    // }
    if(med=='whatsapp')
    {
      this.socialSharing.shareViaWhatsApp("Welcome to India GK. Click here for Share on Whatsapp.", null, this.urllnk).then((result) => {
        alert('result')
       }).catch((err) => {
        alert('error');
        alert(JSON.stringify(err));
        // this.constant.ToastCustom("What's App not installed your device",'bottom');
      }); 
    }
  
  }

}
