import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirstInIndiaPage } from '../first-in-india/first-in-india';
import { StateIndiaPage } from '../state-india/state-india';
import { ImportanceKnowledgeIndiaPage } from '../importance-knowledge-india/importance-knowledge-india';
import { SportsPage } from '../sports/sports';
import { BharatRatnPage } from '../bharat-ratn/bharat-ratn';
import { IplPage } from '../ipl/ipl';
import { IndianGeographyPage } from '../indian-geography/indian-geography';
import { RiverPage } from '../river/river';
import { NovelPage } from '../novel/novel';
import { DiscoveryPage } from '../discovery/discovery';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';

import { SocialSharing } from '@ionic-native/social-sharing';
import { ImportantDaysPage } from '../important-days/important-days';
import { InternationalBorderPage } from '../international-border/international-border';
import { HistoricPlacesPage } from '../historic-places/historic-places';
  




@Component({
  selector: 'page-india',
  templateUrl: 'india.html',
})
export class IndiaPage {
  urllnk:any;
  constructor(public navCtrl: NavController, private socialSharing: SocialSharing, public navParams: NavParams, private admobFree: AdMobFree) {

    this.urllnk="https://play.google.com/store/apps/details?id=com.di.project";

    const bannerConfig: AdMobFreeBannerConfig = {

      id:'ca-app-pub-7299908723756574/2046503015',
      // add your config here
      // for the sake of this example we will just use the test config
      isTesting:false,
      autoShow: true
     };
     this.admobFree.banner.config(bannerConfig);
     
     this.admobFree.banner.prepare()
       .then(() => {
         // banner Ad is ready
         // if we set autoShow to false, then we will need to call the show method here
       })
       .catch(e => console.log(e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SamplePappersPage');

   
  }

  onshare(med:string)
  {
    if(med=='share')
    {
      this.socialSharing.share("share",null,null,"https://play.google.com/store/apps/details?id=com.di.project");
    }
    // if (med == 'gmail') {
    //   // Share via email
    //   this.socialSharing.canShareVia("com.google.android.gm", "Welcome to IBA. Click here for download the IBA App.", "IBA Mobile Application", null,this.urllnk).then(() => {
    //     this.socialSharing.shareVia("com.google.android.gm", "Welcome to IBA. Click here for download the IBA App.", "IBA Mobile Application", null,this.urllnk).then(() => {
    //       console.log("Success");
    //     }).catch((error) => {
    //       this.constant.ToastCustom(" Gmail not installed your device",'bottom');
    //       console.log("error " + JSON.stringify(error));
    //     });
    //   }).catch((error) => {
    //     this.constant.ToastCustom("Gmail not installed your device",'bottom');
    //     console.log("Error, Looks like you don't have Gmail app installed " + JSON.stringify(error) );
    //   });
    // }
    // if(med=='facebook')
    // {
    //   //alert( this.urllnk);
    //   this.socialSharing.shareViaFacebook("Welcome to IBA. Click here for download the IBA App.", null,"https://play.google.com/store/apps/details?id=com.trs.iba").then((result) => {
    //   }).catch((msg) => {
    //     this.constant.ToastCustom("Facebook not installed your device",'bottom');
    //   }); 
    // }
    // if(med=='twitter')
    // {
    //   this.socialSharing.shareViaTwitter("Welcome to IBA. Click here for download the IBA App.",null,  this.urllnk).then((result) => {
    //    }).catch((msg) => {
    //     this.constant.ToastCustom("Twitter not installed your device",'bottom');
    //   }); 
    // }
    if(med=='whatsapp')
    {
      this.socialSharing.shareViaWhatsApp("Share", null, this.urllnk).then((result) => {
        alert('result')
       }).catch((err) => {
        alert('error');
        alert(JSON.stringify(err));
        // this.constant.ToastCustom("What's App not installed your device",'bottom');
      }); 
    }
  
  }

  GetIndiaFirst(){
    this.navCtrl.push(FirstInIndiaPage);
  }

  GetIndiaState(){
    this.navCtrl.push(StateIndiaPage);
  }

  GetImportanceKnowledgeIndia(){
    this.navCtrl.push(ImportanceKnowledgeIndiaPage);
  }

  GetSports(){
    this.navCtrl.push(SportsPage);
  }

  GetBharatratn(){
    this.navCtrl.push(BharatRatnPage);
  }

  GetIpl(){
    this.navCtrl.push(IplPage);
  }

  GetIndianGeography(){
    this.navCtrl.push(IndianGeographyPage);
  }

  GetRiver(){

    this.navCtrl.push(RiverPage);

  }

  GetNovel(){

    this.navCtrl.push(NovelPage);

  }
  GetDiscovery(){

    this.navCtrl.push(DiscoveryPage);

  }

  GetImportantDay(){

    this.navCtrl.push(ImportantDaysPage);

  }

  GetInternationalBorder(){

    this.navCtrl.push(InternationalBorderPage);

  }

  GetIndiaHistoricPlaces(){

    this.navCtrl.push(HistoricPlacesPage);

  }

}
