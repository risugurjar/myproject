import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoricPlacesPage } from './historic-places';

@NgModule({
  declarations: [
    HistoricPlacesPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoricPlacesPage),
  ],
})
export class HistoricPlacesPageModule {}
