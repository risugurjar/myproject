import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InternationalBorderPage } from './international-border';

@NgModule({
  declarations: [
    InternationalBorderPage,
  ],
  imports: [
    IonicPageModule.forChild(InternationalBorderPage),
  ],
})
export class InternationalBorderPageModule {}
