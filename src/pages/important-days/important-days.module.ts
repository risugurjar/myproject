import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImportantDaysPage } from './important-days';

@NgModule({
  declarations: [
    ImportantDaysPage,
  ],
  imports: [
    IonicPageModule.forChild(ImportantDaysPage),
  ],
})
export class ImportantDaysPageModule {}
