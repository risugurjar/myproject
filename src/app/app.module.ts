import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IndiaPage } from '../pages/india/india';
import { FirstInIndiaPage } from '../pages/first-in-india/first-in-india';
import { StateIndiaPage } from '../pages/state-india/state-india';
import { ImportanceKnowledgeIndiaPage } from '../pages/importance-knowledge-india/importance-knowledge-india';
import{ SportsPage} from '../pages/sports/sports';
import { BharatRatnPage } from '../pages/bharat-ratn/bharat-ratn';
import { IplPage } from '../pages/ipl/ipl';
import { IndianGeographyPage} from '../pages/indian-geography/indian-geography';
import { RiverPage } from '../pages/river/river';
import { AdMobFree } from '@ionic-native/admob-free';
import { SocialSharing } from '@ionic-native/social-sharing';

// import { InAppBrowser } from'@ionic-native/in-app-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NovelPage } from '../pages/novel/novel';
import { DiscoveryPage } from '../pages/discovery/discovery';
import { SharePage } from '../pages/share/share';
import { ImportantDaysPage } from '../pages/important-days/important-days';
import { InternationalBorderPage } from '../pages/international-border/international-border';
import { HistoricPlacesPage } from '../pages/historic-places/historic-places'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    IndiaPage,
    FirstInIndiaPage,
    StateIndiaPage,
    ImportanceKnowledgeIndiaPage,
    SportsPage,
    NovelPage,
    BharatRatnPage,
    IplPage,
    IndianGeographyPage,
    RiverPage,
    DiscoveryPage,
    SharePage,
    ImportantDaysPage,
    InternationalBorderPage,
    HistoricPlacesPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    IndiaPage,
    FirstInIndiaPage,
    StateIndiaPage,
    ImportanceKnowledgeIndiaPage,
    SportsPage,
    BharatRatnPage,
    IplPage,
    IndianGeographyPage,
    RiverPage,
    NovelPage,
    DiscoveryPage,
    SharePage,
    ImportantDaysPage,
    InternationalBorderPage,
    HistoricPlacesPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AdMobFree,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser
  ]
})
export class AppModule {}
