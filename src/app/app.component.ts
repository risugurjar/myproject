import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IndiaPage } from '../pages/india/india';
import { FirstInIndiaPage } from '../pages/first-in-india/first-in-india';
import { StateIndiaPage } from '../pages/state-india/state-india';
import { ImportanceKnowledgeIndiaPage } from '../pages/importance-knowledge-india/importance-knowledge-india';
import { SportsPage } from '../pages/sports/sports';
import { BharatRatnPage } from '../pages/bharat-ratn/bharat-ratn';
import { IplPage } from '../pages/ipl/ipl';
import { IndianGeographyPage} from '../pages/indian-geography/indian-geography';
import { RiverPage } from '../pages/river/river';
import { NovelPage} from'../pages/novel/novel';

// import { InAppBrowser } from'@ionic-native/in-app-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SharePage } from '../pages/share/share';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = IndiaPage;

  pages: Array<{title: string, component: any,icon:any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private iab: InAppBrowser) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
     // { title: 'Home', component: HomePage },
       
       { title: 'General Questions', component: IndiaPage, icon: 'assets/icon/questions.svg' },
       { title: 'Share App', component: SharePage, icon:'assets/icon/share.svg' },
       { title: 'More App', component: IndiaPage, icon:'assets/icon/round-add-button.svg' },
       { title: 'Rate Us', component: IndiaPage, icon:'assets/icon/star.svg' },
    // { title: 'Ipl', component: IplPage },
    // { title: 'IndianGeographyPage', component:IndianGeographyPage}
    ];

  }

  openPage(page) {
    let url = JSON.parse(localStorage.getItem("setting"));
    if(page.title == "More App"){
    const browser = this.iab.create("https://play.google.com/store/apps/developer?id=Informative+Dinesh");
    }else if(page.title == "Rate Us"){
      const browser = this.iab.create("https://play.google.com/store/apps/details?id=com.di.project");
    }
    else{
      this.nav.setRoot(page.component);  
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  // openPage(page) {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.nav.setRoot(page.component);
  //}
}
